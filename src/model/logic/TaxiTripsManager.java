package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Nodo;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Taxi;
import model.vo.Item;
import model.vo.Service;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 


	private Stack<Service> pila = new Stack<Service>();

	private Queue<Service> cola = new Queue<Service>();



	//--------------------------------------------------------------------------------------------

	//	private Stack<Service> pilaa;
	//	private Queue<Service> colaa;
	//
	//	public void loadServiceas(String serviceFile, String pTaxiId) {
	//		// TODO Auto-generated method stub
	//		JSONParser parser = new JSONParser();
	//		//inicializar cola y pila
	//
	//		pila = new Stack<Service>();
	//		cola = new Queue<Service>();
	//
	//		//"dropoff_census_tract":"17031839100","dropoff_centroid_latitude":"41.880994471","dropoff_centroid_location":{"type":"Point","coordinates":[-87.6327464887,41.8809944707]},"dropoff_centroid_longitude":"-87.632746489","dropoff_community_area":"32","extras":"0","fare":"6","payment_type":"Credit Card","pickup_census_tract":"17031320100","pickup_centroid_latitude":"41.884987192","pickup_centroid_location":{"type":"Point","coordinates":[-87.6209929134,41.8849871918]},"pickup_centroid_longitude":"-87.620992913","pickup_community_area":"32","taxi_id":"7ed122481c0964a5555309bf4696e25bbf7def086d7ecb94b4910b6129501468f748621f8c51a9e0daaaa8ecca9624925356a53e2860cefdbc3caf730404d9a9","tips":"2","tolls":"0","trip_end_timestamp":"2017-02-01T09:00:00.000","trip_id":"8b6010e14e1be89b90d9c51f9e8d2be14d162a2c","trip_miles":"0","trip_seconds":"300","trip_start_timestamp":"2017-02-01T09:00:00.000","trip_total":"8"}
	//		Long inicio = System.currentTimeMillis();
	//		try
	//		{
	//
	//			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
	//			JSONArray arr= (JSONArray) parser.parse(new FileReader(serviceFile));
	//			Iterator iter = arr.iterator();
	//			int z = 0;
	//			while ( iter.hasNext() )
	//			{
	//				JSONObject objeto = (JSONObject)arr.get(z);
	//				String taxiId = " ";
	//				if(objeto.get("taxi_id")!=null)
	//				{
	//					taxiId = (String) objeto.get("taxi_id");
	//				}
	//
	//				if(taxiId.equals(pTaxiId))
	//				{
	//					String company = "Independiente";
	//					if(objeto.get("company") != null)
	//					{
	//						company = (String) objeto.get("company");
	//					}
	//
	//					String dropoffCensusTract = "000000000000";
	//					if(objeto.get("dropoff_census_tract") != null)
	//					{
	//						dropoffCensusTract = (String) objeto.get("dropoff_census_tract");
	//					}
	//
	//					String dropoffCentroidLatitude = (String)objeto.get("dropoff_centroid_latitude");
	//					ArrayList dropoffCentroidLocationA = new ArrayList(); 
	//					if(objeto.get("dropoff_centroid_location") != null)
	//					{
	//						JSONObject dropoffCentroidLocation = (JSONObject) objeto.get("dropoff_centroid_location");
	//						String tipo = (String)dropoffCentroidLocation.get("type");
	//						JSONArray prueba = (JSONArray) dropoffCentroidLocation.get("coordinates");
	//						Object latitud = prueba.get(0);
	//						Object longitud = prueba.get(1);
	//						dropoffCentroidLocationA.add(latitud);
	//						dropoffCentroidLocationA.add(longitud);
	//						dropoffCentroidLocationA.add(tipo);
	//
	//
	//					}
	//
	//					String dropoffCentroidLongitude =" "; 
	//					if(objeto.get("dropoff_centroid_longitude")!= null)
	//					{
	//						dropoffCentroidLongitude=(String)objeto.get("dropoff_centroid_longitude");
	//					}
	//
	//					int dropoffCommunityArea = -1;
	//					if(objeto.get("dropoff_community_area") != null)
	//					{
	//						dropoffCommunityArea = Integer.parseInt((String)objeto.get("dropoff_community_area"));
	//					}
	//
	//					String extras=" ";
	//					if(objeto.get("extras")!= null)
	//					{
	//						extras = (String) objeto.get("extras");
	//					}
	//
	//					String fare = " ";
	//					if(objeto.get("fare")!=null)
	//					{
	//						fare= (String) objeto.get("fare");
	//					}
	//
	//					String paymentType = " ";
	//					if(objeto.get("payment_type")!=null)
	//					{
	//						paymentType = (String) objeto.get("payment_type");
	//					}
	//
	//					String pickupCensusTract = " ";
	//					if(objeto.get("pickup_census_tract") != null)
	//					{
	//						pickupCensusTract = (String) objeto.get("pickup_census_tract");
	//					}
	//
	//					String pickupCentroidLatitude = " ";
	//					if(objeto.get("pickup_centroid_latitude") != null )
	//					{
	//						pickupCentroidLatitude = (String) objeto.get("pickup_centroid_latitude");
	//					}
	//
	//					ArrayList pickupCentroidLocation = new ArrayList();
	//					if(objeto.get("pickup_centroid_location") != null)
	//					{
	//						JSONObject pickupCentroidLocation1 = (JSONObject) objeto.get("pickup_centroid_location");
	//
	//					}
	//
	//					String pickupCentroidLongitude = " ";
	//					if(objeto.get("pickup_centroid_longitude")!=null)
	//					{
	//						pickupCentroidLongitude = (String) objeto.get("pickup_centroid_longitude");
	//					}
	//
	//					String pickUpCommunityArea = " ";
	//					if(objeto.get("pickup_community_area")!= null)
	//					{
	//						pickUpCommunityArea = (String) objeto.get("pickup_community_area");
	//					}
	//
	//
	//
	//					String tips = " ";
	//					if(objeto.get("tips") != null)
	//					{
	//						tips = (String) objeto.get("tips");
	//					}
	//					String tolls = " ";
	//					if(objeto.get("tolls") !=null)
	//					{
	//						tolls = (String) objeto.get("tolls");
	//					}
	//
	//					String tripEndTimeStamp = " ";
	//					if(objeto.get("trip_end_timestamp") != null)
	//					{
	//						tripEndTimeStamp = (String) objeto.get("trip_end_timestamp");
	//					}
	//
	//					String tripId = " ";
	//					if(objeto.get("trip_id") != null)
	//					{
	//						tripId = (String) objeto.get("trip_id");
	//					}
	//
	//					double tripMiles = 0;
	//					if(objeto.get("trip_miles")!=null)
	//					{
	//						tripMiles = Double.parseDouble((String)objeto.get("trip_miles"));
	//					}
	//
	//					int tripSeconds = 0;
	//					if(objeto.get("trip_seconds")!= null)
	//					{
	//						tripSeconds = Integer.parseInt((String)objeto.get("trip_seconds"));
	//					}
	//
	//					String tripStartTimestamp =" ";
	//					tripStartTimestamp =(String)objeto.get("trip_start_timestamp");
	//					SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss");
	//					Date miFecha = formateador.parse(tripStartTimestamp); 
	//
	//
	//
	//					double tripTotal = 0;
	//					if(objeto.get("trip_total")!= null)
	//					{
	//						tripTotal = Double.parseDouble((String)objeto.get("trip_total"));
	//					}
	//
	//
	//					Service servicioNuevo = new Service(dropoffCensusTract, dropoffCentroidLatitude, dropoffCentroidLongitude, dropoffCentroidLongitude, dropoffCommunityArea, extras, fare, paymentType, pickupCensusTract, pickupCentroidLatitude, pickupCentroidLongitude, pickUpCommunityArea, taxiId, tips, tolls, tripEndTimeStamp, tripId, tripMiles, tripSeconds, miFecha, tripTotal);
	//					pila.push(servicioNuevo);
	//					cola.enqueue(servicioNuevo);
	//
	//				}
	//				iter.next();
	//				z++;
	//			}
	//
	//			System.out.println(pila.darTamanio());
	//			System.out.println(cola.darTamanio());
	//			Long fin = System.currentTimeMillis();
	//
	//			System.out.println(fin-inicio);
	//		}
	//		catch(Exception e)
	//		{
	//			e.printStackTrace();
	//		}
	//	}

	//-----------------------------------------------------------------------------------------------



	@SuppressWarnings("rawtypes")
	public void loadServices(String serviceFile, String taxiId) {
		// TODO Auto-generated method stub
		System.out.println("Inside loadServices with File:" + serviceFile);
		System.out.println("Inside loadServices with TaxiId:" + taxiId);
		Long inicio = System.currentTimeMillis();
		try {
			System.out.println("inicia el try :v");
			JsonReader reader=new JsonReader(new FileReader(serviceFile));
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
			System.out.println("Se prepara para crear el item");
			Item[] item= gson.fromJson(reader, Item[].class);
			System.out.println("crea el item");
			for (int i = 0; i < item.length; i++) {

				String tripStartTimestamp =(String)((List) item[i]).get(item[i].start());
				SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss");
				Date miFecha = formateador.parse(tripStartTimestamp);

				if(item[i].taxiId().equals(taxiId))
				{
					System.out.println("Entra al if");
					Service np = new Service(item[i].taxiId(), miFecha);
					cola.enqueue(np);
					pila.push(np );
				}
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Long fin = System.currentTimeMillis();

		System.out.println("Se demor�: " + (fin-inicio)+ " milisegundos");
		System.out.println("se cargaron...  "+ cola.darTamanio() +" servicios del archivo:" + serviceFile);


	}

	@Override
	public int [] servicesInInverseOrder() {
		// TODO Auto-generated method stub
		System.out.println("Inside servicesInInverseOrder");
		int ordenados = 0;
		int desordenados = 0;
		Service serv;
		Stack<Service> copy = pila;		
		try {

			serv = (Service) copy.pop();
			while(copy.darTope() != null) 
			{				
				if( serv.darHoraServicio().before(copy.darTope().darItem().darHoraServicio()))
				{
					desordenados++;
					copy.pop();
				}
				else 
				{
					ordenados++;
					serv = (Service) copy.pop();
				}
			}
			ordenados++;
		} catch (Exception e) {
			System.out.println("La pila esta vacia");
			e.printStackTrace();
		}
		
		int [] resultados = new int[2];
		resultados[0] = ordenados;
		resultados[1] = desordenados;
		return resultados;
	}

	@Override
	public int [] servicesInOrder() {
		System.out.println("Inside servicesInOrder");
		int ordenados = 0;
		int desordenados = 0;
		Service serv;
		Queue<Service> copy = cola;
		try {

			serv = (Service) copy.dequeue();
			while(copy.darPrimero() != null) 
			{				
				if( !serv.darHoraServicio().before(copy.darPrimero().darItem().darHoraServicio()))
				{
					desordenados++;
					copy.dequeue();
				}
				else 
				{
					ordenados++;
					serv = (Service) copy.dequeue();
				}
			}
			ordenados++;
		} catch (Exception e) {
			System.out.println("La pila esta vacia");
			e.printStackTrace();
		}
		
		int [] resultados = new int[2];
		resultados[0] = ordenados;
		resultados[1] = desordenados;
		return resultados;
	}




}
