package model.vo;

import java.util.ArrayList;

import model.data_structures.Stack;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	
	private String taxi_id;
	
	private String company;
	
	private Stack<Service> servicios;
	
	public void cargarTaxi(String taxiId, String compania) {
		servicios = new Stack<Service>();
		taxi_id = taxiId;
		company = compania;
	}
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return "taxi Id";
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return "company";
	}
	
	@Override
	public int compareTo(Taxi o) {
		// TODO Auto-generated method stub
		return 0;
	}
	public Stack<Service> getServices() {
		// TODO Auto-generated method stub
		return servicios;
	}	
}
