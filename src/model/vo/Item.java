package model.vo;

public class Item {

	public static final String SIN_COM = "Sin Compa�ia";

	private String company;
	
	private String dropoff_census_tract;
	
	private String dropoff_centroid_latitude;
	
	private Object dropoff_centroid_location;
	
	private String trip_id;
	
	private String taxi_id;
		
	private int trip_start_timestamp;
	
	private String trip_end_timestamp;
	
	private String trip_second;
	
	private String trip_miles;
	
	private String pickup_census_tract;
	
	private String pickup_community_area;
	
	private String dropoff_community_area;
	
	private String fare;
	
	private String trips;
	
	private String tolls;
	
	private String extras;
		
	private String trip_total;
	
	private String payment_type;
	
	private String pickup_centroid_latitude;
	
	private String pickup_centroid_longitude;
	
	private Object pickup_centroid_location;
		
	private String dropoff_centroid_longitude;
	
	public String compa�ia(){
		return company;
	}
	
	public void camCompa�ia( String pCompany) {
		company = pCompany;
	}
	
	public String dropT(){
		return dropoff_census_tract;
	}
	
	public String dropLa(){
		return dropoff_centroid_latitude;
	}
	
	public String dropLo(){
		return (String) dropoff_centroid_location;
	}
	
	public String taxiId(){
		return taxi_id;
	}
	
	public String tripId(){
		return trip_id;
	}
	
	public int start(){
		return trip_start_timestamp;
	}
	
	public String end(){
		return trip_end_timestamp;
	}
	
	public String time(){
		return trip_second;
	}
	
	public String miles(){
		return trip_miles;
	}
	
	public String pickT(){
		return pickup_census_tract;
	}
	
	public String pickA(){
		return pickup_community_area;
	}
	
	public String dropA()
	{
		return dropoff_community_area;
	}
	
	public String fare()
	{
		return fare;
	}
	
	public String trips()
	{
		return trips;
	}
	
	public String tolls()
	{
		return tolls;
	}
	
	public String extras()
	{
		return extras;
	}
	
	public String tripT()
	{
		return trip_total;
	}
	
	public String payment()
	{
		return payment_type;
	}
	
	public String pickupLa()
	{
		return pickup_centroid_latitude;
	}
	
	public String pickupLo()
	{
		return pickup_centroid_longitude;
	}
	
	public String pickupLoc()
	{
		return (String) pickup_centroid_location;
	}
	
	public String dropL()
	{
		return dropoff_centroid_longitude;
	}
}
