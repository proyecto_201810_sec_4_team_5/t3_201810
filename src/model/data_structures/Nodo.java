package model.data_structures;

public class Nodo<T> {
	
	private Nodo<T> next; 

	private T item;
	
	public Nodo (Comparable lo)
	{
		this.item = (T)lo;
		this.next = null;
	}
	
	public void cambiarSiguiente(Nodo<T> li)
	{
		next = li;
	}
	
	public Nodo darSiguiente()
	{
		return next;
	}
	
	public T darItem()
	{
		return item;
	}
	
	public void cambiarItem(T p)
	{
		item = p;
	}

	public void cambiarNext(Nodo naw) {
		// TODO Auto-generated method stub
		
	}
	
	
	
}
