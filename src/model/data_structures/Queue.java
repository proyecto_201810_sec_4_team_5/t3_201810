package model.data_structures;

public class Queue<T extends Comparable> implements IQueue {

	private Nodo<T> primero;
	
	private Nodo <T> ultimo;
	
	public int tamanio;
	
	public Queue()
	{
		primero = null;
		ultimo = null; 
		tamanio = 0;
	}
	
	
	@Override
	public void enqueue(Object item) {
		// TODO Auto-generated method stub
		Nodo nodito = new Nodo<T>((Comparable)item);
		if(tamanio==0)
		{
			primero = nodito;
			ultimo = nodito;
		}
		else
		{
			Nodo<T> temp = ultimo;
			temp.cambiarSiguiente(nodito);
			ultimo=nodito;
		}
		tamanio++;
	}

	@Override
	public Object dequeue() throws QueueEmpty{
		// TODO Auto-generated method stub
		if(isEmpty())
		{
			throw new QueueEmpty();
		}
		Nodo<T> viejito = primero;
		T elem = viejito.darItem();
		primero = viejito.darSiguiente();
		viejito.cambiarSiguiente(null);
		if(primero == null)
			ultimo = null;
		tamanio--;
		return elem;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return tamanio==0;
	}
	
	
	public int darTamanio()
	{
		return tamanio;
	}
	
	public Nodo<T> darPrimero(){
		return primero;
	}

}
