package model.data_structures;

public class Stack <T extends Comparable> implements IStack {

	private Nodo<T>tope;
	
	private int tamanio;
	
	public Stack()
	{
		tope = null;
		tamanio = 0;
	}

	@Override
	public void push(Object item) {
		// TODO Auto-generated method stub
		Nodo<T> nodillo = new Nodo<T>((Comparable)item);
		if(isEmpty())
		{
			tope = nodillo;
		}
		else 
		{
			nodillo.cambiarSiguiente(tope);
			tope = nodillo;
		}
		tamanio++;
		
	}

	@Override
	public Object pop() throws StackEmptyException{
		// TODO Auto-generated method stub
		if(isEmpty())
		{
			throw new StackEmptyException();
		}
		
		Nodo<T>viejito = tope;
		T element = viejito.darItem();
		tope = viejito.darSiguiente();
		viejito.cambiarSiguiente(null);
		tamanio--;
		return element;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return tamanio==0;
	}
	
	public int darTamanio()
	{
		return tamanio;
	}
	
	public Nodo<T> darTope()
	{
		return tope;
	}
	
}
